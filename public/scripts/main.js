let myImage = document.querySelector('img');

myImage.onclick = function() {
    let mySrc = myImage.getAttribute('src');
    if(mySrc === 'images/me-and-odie.jpg') {
        myImage.setAttribute ('src','images/odie.jpg');
    } else {
        myImage.setAttribute ('src','images/me-and-odie.jpg');
    }
};

let myButton = document.querySelector('button');
let myHeading = document.querySelector('h1');

function setUserName() {
    let myName = prompt('Please enter your name.');
    if (!myName || myName == null) {
        alert("Must enter a name!");
        setUserName();
    } else {
        localStorage.setItem('name', myName);
        myHeading.textContent = 'Dogs are cool, ' + myName;
    }
};

// Check if the user's name is stored (via localStorage - web storage API)
// and then either get and set it or set the title
if(!localStorage.getItem('name')) {
    setUserName();
} else {
    let storedName = localStorage.getItem('name');
    myHeading.textContent = 'Dogs are cool, ' + storedName;
}

myButton.onclick = function() {
    setUserName();
};
